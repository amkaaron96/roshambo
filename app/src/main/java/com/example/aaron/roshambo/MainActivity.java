package com.example.aaron.roshambo;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity implements View.OnClickListener {

    private Button rockButton;
    private Button paperButton;
    private Button scissorsButton;
    private TextView winText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rockButton = findViewById(R.id.rock);
        paperButton = findViewById(R.id.paper);
        scissorsButton = findViewById(R.id.scissors);
        winText = findViewById(R.id.textView2);

        rockButton.setOnClickListener(this);
        paperButton.setOnClickListener(this);
        scissorsButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view){
        int getNum = getRand();

        switch(view.getId()){
            case R.id.rock:
                if(getNum == 0){
                    winText.setText("You tied!");
                }else if(getNum == 1){
                    winText.setText("You lost!");
                }else if(getNum == 2){
                    winText.setText("You won!");
                }
                break;
            case R.id.paper:
                if(getNum == 0){
                    winText.setText("You won!");
                }else if(getNum == 1){
                    winText.setText("You tied!");
                }else if(getNum == 2){
                    winText.setText("You lost!");
                }
                break;
            case R.id.scissors:
                if(getNum == 0){
                    winText.setText("You lost!");
                }else if(getNum == 1){
                    winText.setText("You won!");
                }else if(getNum == 2){
                    winText.setText("You tied!");
                }
                break;
        }
    }

    public int getRand(){
        //0 is rock, 1 is paper, 2 is scissors

        double randomNum;

        randomNum = Math.random()*2;

        return (int)randomNum;
    }
}
